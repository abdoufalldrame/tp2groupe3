package tp2groupe3.m2retel.tp2groupe3.rest;

import jakarta.validation.Valid;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tp2groupe3.m2retel.tp2groupe3.model.UtilisateursDTO;
import tp2groupe3.m2retel.tp2groupe3.service.UtilisateursService;


@RestController
@RequestMapping(value = "/api/utilisateurss", produces = MediaType.APPLICATION_JSON_VALUE)
public class UtilisateursResource {

    private final UtilisateursService utilisateursService;

    public UtilisateursResource(final UtilisateursService utilisateursService) {
        this.utilisateursService = utilisateursService;
    }

    @GetMapping
    public ResponseEntity<List<UtilisateursDTO>> getAllUtilisateurss() {
        return ResponseEntity.ok(utilisateursService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UtilisateursDTO> getUtilisateurs(
            @PathVariable(name = "id") final Long id) {
        return ResponseEntity.ok(utilisateursService.get(id));
    }

    @PostMapping
    public ResponseEntity<Long> createUtilisateurs(
            @RequestBody @Valid final UtilisateursDTO utilisateursDTO) {
        final Long createdId = utilisateursService.create(utilisateursDTO);
        return new ResponseEntity<>(createdId, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Long> updateUtilisateurs(@PathVariable(name = "id") final Long id,
            @RequestBody @Valid final UtilisateursDTO utilisateursDTO) {
        utilisateursService.update(id, utilisateursDTO);
        return ResponseEntity.ok(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUtilisateurs(@PathVariable(name = "id") final Long id) {
        utilisateursService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
