package tp2groupe3.m2retel.tp2groupe3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Tp2groupe3Application {

    public static void main(final String[] args) {
        SpringApplication.run(Tp2groupe3Application.class, args);
    }

}
