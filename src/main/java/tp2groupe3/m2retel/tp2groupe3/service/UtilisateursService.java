package tp2groupe3.m2retel.tp2groupe3.service;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tp2groupe3.m2retel.tp2groupe3.domain.Utilisateurs;
import tp2groupe3.m2retel.tp2groupe3.model.UtilisateursDTO;
import tp2groupe3.m2retel.tp2groupe3.repos.UtilisateursRepository;
import tp2groupe3.m2retel.tp2groupe3.util.NotFoundException;


@Service
public class UtilisateursService {

    private final UtilisateursRepository utilisateursRepository;

    public UtilisateursService(final UtilisateursRepository utilisateursRepository) {
        this.utilisateursRepository = utilisateursRepository;
    }

    public List<UtilisateursDTO> findAll() {
        final List<Utilisateurs> utilisateurses = utilisateursRepository.findAll(Sort.by("id"));
        return utilisateurses.stream()
                .map(utilisateurs -> mapToDTO(utilisateurs, new UtilisateursDTO()))
                .toList();
    }

    public UtilisateursDTO get(final Long id) {
        return utilisateursRepository.findById(id)
                .map(utilisateurs -> mapToDTO(utilisateurs, new UtilisateursDTO()))
                .orElseThrow(NotFoundException::new);
    }

    public Long create(final UtilisateursDTO utilisateursDTO) {
        final Utilisateurs utilisateurs = new Utilisateurs();
        mapToEntity(utilisateursDTO, utilisateurs);
        return utilisateursRepository.save(utilisateurs).getId();
    }

    public void update(final Long id, final UtilisateursDTO utilisateursDTO) {
        final Utilisateurs utilisateurs = utilisateursRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(utilisateursDTO, utilisateurs);
        utilisateursRepository.save(utilisateurs);
    }

    public void delete(final Long id) {
        utilisateursRepository.deleteById(id);
    }

    private UtilisateursDTO mapToDTO(final Utilisateurs utilisateurs,
            final UtilisateursDTO utilisateursDTO) {
        utilisateursDTO.setId(utilisateurs.getId());
        utilisateursDTO.setNom(utilisateurs.getNom());
        utilisateursDTO.setPrenom(utilisateurs.getPrenom());
        utilisateursDTO.setDateDeNaissance(utilisateurs.getDateDeNaissance());
        utilisateursDTO.setLieuDeNaissance(utilisateurs.getLieuDeNaissance());
        utilisateursDTO.setEmail(utilisateurs.getEmail());
        utilisateursDTO.setTelephone(utilisateurs.getTelephone());
        return utilisateursDTO;
    }

    private Utilisateurs mapToEntity(final UtilisateursDTO utilisateursDTO,
            final Utilisateurs utilisateurs) {
        utilisateurs.setNom(utilisateursDTO.getNom());
        utilisateurs.setPrenom(utilisateursDTO.getPrenom());
        utilisateurs.setDateDeNaissance(utilisateursDTO.getDateDeNaissance());
        utilisateurs.setLieuDeNaissance(utilisateursDTO.getLieuDeNaissance());
        utilisateurs.setEmail(utilisateursDTO.getEmail());
        utilisateurs.setTelephone(utilisateursDTO.getTelephone());
        return utilisateurs;
    }

    public boolean emailExists(final String email) {
        return utilisateursRepository.existsByEmailIgnoreCase(email);
    }

    public boolean telephoneExists(final Long telephone) {
        return utilisateursRepository.existsByTelephone(telephone);
    }

}
