package tp2groupe3.m2retel.tp2groupe3.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tp2groupe3.m2retel.tp2groupe3.model.UtilisateursDTO;
import tp2groupe3.m2retel.tp2groupe3.service.UtilisateursService;
import tp2groupe3.m2retel.tp2groupe3.util.WebUtils;


@Controller
@RequestMapping("/utilisateurss")
public class UtilisateursController {

    private final UtilisateursService utilisateursService;

    public UtilisateursController(final UtilisateursService utilisateursService) {
        this.utilisateursService = utilisateursService;
    }

    @GetMapping
    public String list(final Model model) {
        model.addAttribute("utilisateurses", utilisateursService.findAll());
        return "utilisateurs/list";
    }

    @GetMapping("/add")
    public String add(@ModelAttribute("utilisateurs") final UtilisateursDTO utilisateursDTO) {
        return "utilisateurs/add";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute("utilisateurs") @Valid final UtilisateursDTO utilisateursDTO,
            final BindingResult bindingResult, final RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "utilisateurs/add";
        }
        utilisateursService.create(utilisateursDTO);
        redirectAttributes.addFlashAttribute(WebUtils.MSG_SUCCESS, WebUtils.getMessage("utilisateurs.create.success"));
        return "redirect:/utilisateurss";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable(name = "id") final Long id, final Model model) {
        model.addAttribute("utilisateurs", utilisateursService.get(id));
        return "utilisateurs/edit";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable(name = "id") final Long id,
            @ModelAttribute("utilisateurs") @Valid final UtilisateursDTO utilisateursDTO,
            final BindingResult bindingResult, final RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "utilisateurs/edit";
        }
        utilisateursService.update(id, utilisateursDTO);
        redirectAttributes.addFlashAttribute(WebUtils.MSG_SUCCESS, WebUtils.getMessage("utilisateurs.update.success"));
        return "redirect:/utilisateurss";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") final Long id,
            final RedirectAttributes redirectAttributes) {
        utilisateursService.delete(id);
        redirectAttributes.addFlashAttribute(WebUtils.MSG_INFO, WebUtils.getMessage("utilisateurs.delete.success"));
        return "redirect:/utilisateurss";
    }

}
