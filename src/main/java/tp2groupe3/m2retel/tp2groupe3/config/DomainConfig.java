package tp2groupe3.m2retel.tp2groupe3.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("tp2groupe3.m2retel.tp2groupe3.domain")
@EnableJpaRepositories("tp2groupe3.m2retel.tp2groupe3.repos")
@EnableTransactionManagement
public class DomainConfig {
}
