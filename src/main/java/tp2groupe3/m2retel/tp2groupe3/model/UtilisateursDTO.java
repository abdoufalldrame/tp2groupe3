package tp2groupe3.m2retel.tp2groupe3.model;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;


public class UtilisateursDTO {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String nom;

    @NotNull
    @Size(max = 30)
    private String prenom;

    @NotNull
    private LocalDate dateDeNaissance;

    @NotNull
    @Size(max = 30)
    private String lieuDeNaissance;

    @NotNull
    @Size(max = 60)
    @UtilisateursEmailUnique
    private String email;

    @NotNull
    @UtilisateursTelephoneUnique
    private Long telephone;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(final LocalDate dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getLieuDeNaissance() {
        return lieuDeNaissance;
    }

    public void setLieuDeNaissance(final String lieuDeNaissance) {
        this.lieuDeNaissance = lieuDeNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Long getTelephone() {
        return telephone;
    }

    public void setTelephone(final Long telephone) {
        this.telephone = telephone;
    }

}
