package tp2groupe3.m2retel.tp2groupe3.model;

import jakarta.validation.constraints.Size;


public class Tp2groupe3 {

    @Size(max = 255)
    private String utilisateursDTO;

    public String getUtilisateursDTO() {
        return utilisateursDTO;
    }

    public void setUtilisateursDTO(final String utilisateursDTO) {
        this.utilisateursDTO = utilisateursDTO;
    }

}
