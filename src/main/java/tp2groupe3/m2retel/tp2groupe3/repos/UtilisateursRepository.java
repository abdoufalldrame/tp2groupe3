package tp2groupe3.m2retel.tp2groupe3.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import tp2groupe3.m2retel.tp2groupe3.domain.Utilisateurs;


public interface UtilisateursRepository extends JpaRepository<Utilisateurs, Long> {

    boolean existsByEmailIgnoreCase(String email);

    boolean existsByTelephone(Long telephone);

}
